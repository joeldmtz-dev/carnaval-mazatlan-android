package com.example.carnavalmazatlan.utils;

import android.content.Context;

/**
 * Created by joeldmtz on 12/30/16.
 */

public class DimenUtils {

    Context context;

    public DimenUtils(Context context) {
        this.context = context;
    }

    public int dpToPx(int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
}
