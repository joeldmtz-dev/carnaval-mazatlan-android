package com.example.carnavalmazatlan.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.carnavalmazatlan.MyApplication;
import com.example.carnavalmazatlan.fragments.ChildFragment;
import com.example.carnavalmazatlan.fragments.FemaleFragment;
import com.example.carnavalmazatlan.fragments.MaleFragment;
import com.example.carnavalmazatlan.introslides.CandidateSelectionListener;
import com.example.carnavalmazatlan.introslides.IntroFinishListener;
import com.example.carnavalmazatlan.introslides.SocialLoginFragment;
import com.github.paolorotolo.appintro.AppIntro;

public class IntroAcivity extends AppIntro implements CandidateSelectionListener, IntroFinishListener {

    private int female_selected = -1, male_selected = -1, child_selected = -1;
    private static int femaleIndex = -1, maleIndex = -1, childIndex = -1;

    SocialLoginFragment loginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences(getApplicationContext().getPackageName(), MODE_PRIVATE);
        boolean isVoted = preferences.getBoolean(MyApplication.VOTED, false);

        if (!isVoted) {
            addSlide(FemaleFragment.newInstance(this, femaleIndex));
            addSlide(MaleFragment.newInstance(this, childIndex));
            addSlide(ChildFragment.newInstance(this, childIndex));
        }

        loginFragment = SocialLoginFragment.newInstance(this, isVoted);
        loginFragment.disableVote();
        addSlide(loginFragment);

        showSkipButton(false);
        setProgressButtonEnabled(false);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent().setClass(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSelection(int type, int id, int position) {
        switch (type) {
            case CandidateSelectionListener.FEMALE:
                female_selected = id;
                femaleIndex = position;
                //pager.setCurrentItem(1);
                break;
            case CandidateSelectionListener.MALE:
                male_selected = id;
                maleIndex = position;
                //pager.setCurrentItem(2);
                break;
            case CandidateSelectionListener.CHILD:
                child_selected = id;
                childIndex = position;
                //pager.setCurrentItem(3);
                break;
        }

        if (female_selected >= 0 && male_selected >= 0 && child_selected >= 0) {
            loginFragment.enableVote();
        }
    }

    public void onVoteSend(){

    }

    @Override
    public void onLogin() {
        setProgressButtonEnabled(true);
    }

    @Override
    public void onLogout() {
        setProgressButtonEnabled(false);
    }

    @Override
    public void onSkip() {

    }
}
