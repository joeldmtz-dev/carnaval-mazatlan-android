package com.example.carnavalmazatlan.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.app.Events;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class EventActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        final Events event = intent.getParcelableExtra("event");

        final TextView tvEventName = (TextView) findViewById(R.id.tvEventName);
        tvEventName.setText(event.getEventName());

        final TextView tvEventDate = (TextView) findViewById(R.id.tvEventDate);
        tvEventDate.setText(event.getEventDate());

        final TextView tvEventTime = (TextView) findViewById(R.id.tvEventTime);
        tvEventTime.setText(event.getEventTime());

        final TextView tvEventLocation = (TextView) findViewById(R.id.tvEventLocation);
        tvEventLocation.setText(event.getEventLocation());

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapseToolbar);
        collapsingToolbarLayout.setTitle(" ");

        MapFragment city_map = (MapFragment) getFragmentManager().findFragmentById(R.id.city_map);
        city_map.getMapAsync(this);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Events> eventsList = Events.findWithQuery(Events.class,
                        "SELECT * FROM Events WHERE event_name = ?", event.getEventName());
                if (eventsList.size() == 0) {
                    event.save();
                    Toast.makeText(getApplicationContext(), event.getEventName() + " agregado a su lista de eventos.",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Ya ha agregado " + event.getEventName() + " a su lista de eventos.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(23.198286, -106.422675))
        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23.198286, -106.422675), 17));
        marker.getPosition();
    }
}
