package com.example.carnavalmazatlan.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.MoreAdapter;
import com.example.carnavalmazatlan.classes.app.More;
import com.example.carnavalmazatlan.classes.app.Notification;
import com.example.carnavalmazatlan.services.NotificationService;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    private final Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Configuraciones");
        setContentView(R.layout.activity_settings);

        final ListView settingsList = (ListView) findViewById(R.id.settingsList);
        ArrayList<More> data = new ArrayList<>();
        String notifyText = (NotificationService.DAYS) ?
                (NotificationService.NOTIFICATION_REMINDER == 1) ? "1 día antes" : "2 días antes" : "1 hora antes";
        data.add(new More("Notificaciones", notifyText));
        data.add(new More("Cambiar candidato", "Cambia tu voto"));
        final MoreAdapter moreAdapter = new MoreAdapter(ctx, R.layout.more_item, data);
        settingsList.setAdapter(moreAdapter);
        settingsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        PopupMenu popupMenu = new PopupMenu(ctx, view);
                        popupMenu.getMenuInflater().inflate(R.menu.notification_popup, popupMenu.getMenu());
                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()){
                                    case R.id.opc_1: {
                                        //Una hora antes del evento
                                        NotificationService.NOTIFICATION_REMINDER = 1;
                                        NotificationService.DAYS = false; //En horas
                                        Notification notification = Notification.findById(Notification.class, 1);
                                        notification.setDays(0);
                                        notification.setNotificationReminder(1);
                                        notification.save();
                                        break;
                                    }
                                    case R.id.opc_2: {
                                        //Un día antes del evento
                                        NotificationService.NOTIFICATION_REMINDER = 1;
                                        NotificationService.DAYS = true; //En días
                                        Notification notification = Notification.findById(Notification.class, 1);
                                        notification.setDays(1);
                                        notification.setNotificationReminder(1);
                                        notification.save();
                                        break;
                                    }
                                    case R.id.opc_3: {
                                        //Dos días antes del evento
                                        NotificationService.NOTIFICATION_REMINDER = 2;
                                        NotificationService.DAYS = true; //En días
                                        Notification notification = Notification.findById(Notification.class, 1);
                                        notification.setDays(1);
                                        notification.setNotificationReminder(2);
                                        notification.save();
                                        break;
                                    }
                                }
                                moreAdapter.getItem(0).setSubtitle(item.getTitle().toString());
                                moreAdapter.notifyDataSetChanged();
                                return true;
                            }
                        });
                        popupMenu.show();
                        break;
                    case 1: //Cambiar voto
                        break;
                }
            }
        });
    }
}