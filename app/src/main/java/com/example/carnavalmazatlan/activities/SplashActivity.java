package com.example.carnavalmazatlan.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.carnavalmazatlan.MyApplication;
import com.example.carnavalmazatlan.R;
import com.facebook.AccessToken;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "iS7JZB4vBGC3qFtgnEKxuVHg5";
    private static final String TWITTER_SECRET = "ZPm4qWBMYlotKWHtj4Tj0Dh0I0Xq5FRp3NyRM5s53uoKP6oAwl";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_splash);

        SharedPreferences preferences = getSharedPreferences(getApplicationContext().getPackageName(), MODE_PRIVATE);

        Intent intent;
        if (!preferences.getBoolean(MyApplication.VOTED, false) || AccessToken.getCurrentAccessToken() == null){
            intent = new Intent(this, IntroAcivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);
        }

        startActivity(intent);

        finish();
    }
}