package com.example.carnavalmazatlan.introslides;

/**
 * Created by joeldmtz on 1/2/17.
 */

public interface IntroFinishListener {
    void onVoteSend();
    void onLogin();
    void onLogout();
    void onSkip();
}
