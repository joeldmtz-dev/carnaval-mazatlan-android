package com.example.carnavalmazatlan.introslides;

/**
 * Created by joeldmtz on 12/31/16.
 */

public interface CandidateSelectionListener {

    int FEMALE = 0, MALE = 1, CHILD = 2;

    void onSelection(int type, int id, int position);
}
