package com.example.carnavalmazatlan.introslides;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.carnavalmazatlan.MyApplication;
import com.example.carnavalmazatlan.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by joeldmtz on 12/14/16.
 */

public class SocialLoginFragment extends Fragment {

    CallbackManager callbackManager;

    @BindView(R.id.layout_vote)
    LinearLayout layout_vote;

    @BindView(R.id.btn_vote)
    Button btn_vote;

    @BindView(R.id.layout_social_login)
    LinearLayout layout_social_login;

    @BindView(R.id.login_button)
    LoginButton loginButton;

    @BindView(R.id.btn_skip_login)
    Button skipLogin;

    private IntroFinishListener listener;
    private boolean voted = false;
    private boolean isVotedEnabled;

    AccessTokenTracker accessTokenTracker;

    public SocialLoginFragment() {
    }

    public static SocialLoginFragment newInstance() {
        return new SocialLoginFragment();
    }

    public static SocialLoginFragment newInstance(IntroFinishListener listener, boolean voted){
        SocialLoginFragment fragment = new SocialLoginFragment();
        fragment.setListener(listener);
        fragment.setVoted(voted);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }

    public void setListener(IntroFinishListener listener) {
        this.listener = listener;
    }

    public void enableVote(){
        isVotedEnabled = true;
        if(btn_vote != null ){
            btn_vote.setEnabled(true);
        }
    }

    public void disableVote(){
        isVotedEnabled = false;
        if(btn_vote != null ){
            btn_vote.setEnabled(false);
        }
    }

    public boolean isVoted() {
        return voted;
    }

    public void setVoted(boolean voted) {
        this.voted = voted;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        if (isVoted()) {
            layout_vote.setVisibility(View.GONE);
        } else {
            layout_social_login.setVisibility(View.GONE);
        }

        // Layout Vote
        btn_vote.setEnabled(isVotedEnabled);
        btn_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferences = getApplicationContext().getSharedPreferences(
                        getApplicationContext().getPackageName(),
                        Context.MODE_PRIVATE
                );

                SharedPreferences.Editor editor = preferences.edit().putBoolean(MyApplication.VOTED, true);
                editor.apply();

                Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);

                if(layout_social_login.getVisibility()==View.GONE){

                    layout_vote.startAnimation(fadeOut);
                    layout_vote.setVisibility(View.GONE);

                    layout_social_login.startAnimation(slideUp);
                    layout_social_login.setVisibility(View.VISIBLE);
                }

                if (listener != null) {
                    listener.onVoteSend();
                }
            }
        });


        // Layout Social Login
        loginButton.setReadPermissions("public_profile");
        loginButton.setFragment(this);

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null && listener != null) {
                    listener.onLogout();
                }
            }
        };

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                if (listener != null ){
                    listener.onLogin();
                    skipLogin.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
            }
        });

        skipLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onSkip();
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (accessTokenTracker != null) {
            accessTokenTracker.stopTracking();
        }
    }
}
