package com.example.carnavalmazatlan.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.activities.EventActivity;
import com.example.carnavalmazatlan.classes.app.Events;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

public class NotificationService extends Service{

    public static int NOTIFICATION_REMINDER = 1; //Variable para saber cuando notificar
    public static  boolean DAYS = true; //Variable para saber si notificar en horas o en días
    private Runnable mRunnable;
    private List<Long> notifiedEvents = new ArrayList<>();
    private Handler mHandler;

    public NotificationService(Context applicationContext){
        super();
        Log.i("HERE", "here I am!");
    }
    public NotificationService(){}

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("START", "onStartCommand");
        checkNotification();
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "onDestroy!");
        sendBroadcast(new Intent("com.example.carnavalmazatlan.RESTART_SERVICE"));
        stopNotification();
    }

    private void showNotification(Events event){
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setContentTitle("Carnaval Mazatlán");
        notification.setContentText("Este es un recordatorio para " + event.getEventName() + ". ¡No te lo pierdas!");
        notification.setAutoCancel(true);

        Intent intent = new Intent(this, EventActivity.class);
        intent.putExtra("event", event);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, event.getId().intValue(), intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //Generar número al azár para mostrar múltiples notificaciones con diferente ID
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        notificationManager.notify(m, notification.build());
    }

    private void stopNotification(){
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
            mHandler = null;
        }
    }

    private void checkNotification(){
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                List<Events> eventsList = Events.listAll(Events.class);
                for (Events event : eventsList){
                    if(!notifiedEvents.contains(event.getId())) {
                        try {
                            SimpleDateFormat simpleDateFormat = (DAYS) ? new SimpleDateFormat("dd/MM/yyyy")
                                    : new SimpleDateFormat("HH:mm");
                            Calendar eventCalendar = GregorianCalendar.getInstance();
                            Calendar nowCalendar = GregorianCalendar.getInstance();
                            if (DAYS) {
                                Date eventDate = simpleDateFormat.parse(event.getEventDate());
                                eventCalendar.setTime(eventDate);
                                int dayOfYearEvent = eventCalendar.get(Calendar.DAY_OF_YEAR);

                                nowCalendar.setTime(new Date());
                                int dayOfYearNow = nowCalendar.get(Calendar.DAY_OF_YEAR);

                                if ((dayOfYearEvent - dayOfYearNow) == NOTIFICATION_REMINDER) {
                                    showNotification(event);
                                    notifiedEvents.add(event.getId());
                                }
                            } else {
                                Date eventTime = simpleDateFormat.parse(event.getEventTime());
                                eventCalendar.setTime(eventTime);
                                int timeEvent = eventCalendar.get(Calendar.HOUR_OF_DAY);

                                nowCalendar.setTime(new Date());
                                int timeNow = nowCalendar.get(Calendar.HOUR_OF_DAY);

                                if ((timeEvent - timeNow) == NOTIFICATION_REMINDER) {
                                    showNotification(event);
                                    notifiedEvents.add(event.getId());
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mHandler.postDelayed(mRunnable, 15 * 1000); //Un minuto
            }
        };
        mHandler.postDelayed(mRunnable, 15 * 1000);
    }
}