package com.example.carnavalmazatlan.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class RestartService extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("RESTARTSERVICE", "onReceive()");
        context.startService(new Intent(context, NotificationService.class));
    }
}
