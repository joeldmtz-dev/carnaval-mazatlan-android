package com.example.carnavalmazatlan.custom_components;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.facebook.Attachment;
import com.example.carnavalmazatlan.classes.facebook.Post;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/20/16.
 */

public class VideoPreview extends LinearLayout implements ExoPlayer.EventListener{

    Context context;
    VideoLoadListener listener;

    @BindView(R.id.preview_container)
    RelativeLayout preview_container;

    @BindView(R.id.img_preview)
    ImageView img_preview;

    @BindView(R.id.video_attachment)
    SimpleExoPlayerView video_attachment;

    private String source = null;
    private SimpleExoPlayer player = null;
    private DataSource.Factory dataSourceFactory = null;
    private ExtractorsFactory extractorsFactory = null;

    public VideoPreview(Context context) {
        super(context);
        initialize(context);
    }

    public VideoPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public VideoPreview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VideoPreview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context);
    }

    public void initialize(Context context){
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.widget_video_preview_player, (ViewGroup) getRootView(), true);
        ButterKnife.bind(this, view);

        video_attachment.setVisibility(View.GONE);
    }

    public void setUpPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory){
        this.player = player;
        this.dataSourceFactory = dataSourceFactory;
        this.extractorsFactory = extractorsFactory;
    }

    public void setPlayerView(SimpleExoPlayer player){
        video_attachment.setPlayer(player);
    }

    public void setMediaContent(final Post post){
        Attachment attachment = post.attachments.data.get(0);
        if(attachment != null && attachment.media != null){
            Picasso.with(context)
                    .load(attachment.media.image.src)
                    .into(img_preview);
        } else {
            img_preview.setVisibility(View.GONE);
        }

        img_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadVideo();
            }
        });

        source = post.source;
        this.player.addListener(this);
    }

    public void setVideLoadlistener(VideoLoadListener listener){
        this.listener = listener;
    }

    public void loadVideo(){
        if (source != null && player != null){

            // This is the MediaSource representing the media to be played.
            Uri uri = Uri.parse(source);
            MediaSource videoSource = new ExtractorMediaSource(uri, dataSourceFactory, extractorsFactory, null, null);

            // Prepare the player with the source.
            player.prepare(videoSource);

            if (this.listener != null){
                listener.onLoadVideo(this);
            }
        }
    }

    public void showVideo(){
        preview_container.setVisibility(View.GONE);
        video_attachment.setVisibility(View.VISIBLE);
    }

    public void resetState(){
        preview_container.setVisibility(View.VISIBLE);
        video_attachment.setVisibility(View.GONE);
        video_attachment.setPlayer(null);
        //this.player.removeListener(this);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_READY) {
            if (listener != null) {
                listener.onPlayVideo();
            } else {
                showVideo();
            }
        }
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity() {

    }
}
