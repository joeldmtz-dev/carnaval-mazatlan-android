package com.example.carnavalmazatlan.custom_components;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.carnavalmazatlan.R;

public class ExpandableTextView extends LinearLayout {

    private at.blogc.android.views.ExpandableTextView textView;
    private TextView button;
    private LinearLayout expander;

    public ExpandableTextView(Context context) {
        super(context);
        initialize(context);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public ExpandableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    @TargetApi(21)
    public ExpandableTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context);
    }

    private void initialize(Context context) {
        LayoutInflater.from(context).inflate(R.layout.widget_expandable_textview, (ViewGroup) getRootView(), true);

        textView = (at.blogc.android.views.ExpandableTextView) findViewById(R.id.expanderText);
        textView.setAnimationDuration(1000L);
        textView.setInterpolator(new OvershootInterpolator());
        textView.setOnExpandListener(new at.blogc.android.views.ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(at.blogc.android.views.ExpandableTextView view) {
                button.setText(R.string.less);
            }

            @Override
            public void onCollapse(at.blogc.android.views.ExpandableTextView view) {
                button.setText(R.string.more);
            }
        });
        button = (TextView) findViewById(R.id.expanderMoreButton);
        expander = (LinearLayout) findViewById(R.id.expander);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.toggle();
            }
        });
    }

    public TextView getTextView(){
        return textView;
    }

    public void setText(CharSequence text) {
        addGlobalListener();
        textView.setText(text);
    }

    public void refreshState(){
        if (textView.isExpanded()) {
            textView.collapse();
        }
    }

    private void addGlobalListener() {
        if (textView == null) return;
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (textView.getLineCount() <= textView.getMaxLines()) {
                    button.setVisibility(GONE);
                } else {
                    button.setVisibility(VISIBLE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }
}