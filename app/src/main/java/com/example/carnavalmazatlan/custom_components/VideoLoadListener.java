package com.example.carnavalmazatlan.custom_components;

import android.view.View;

/**
 * Created by joeldmtz on 12/20/16.
 */

public interface VideoLoadListener {
    void onLoadVideo(View holder);
    void onPlayVideo();
}
