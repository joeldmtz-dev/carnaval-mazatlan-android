package com.example.carnavalmazatlan.custom_components;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.facebook.Attachment;
import com.example.carnavalmazatlan.classes.facebook.Post;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/20/16.
 */

public class LinkPreviewView extends LinearLayout {

    @BindView(R.id.link_container)
    LinearLayout link_container;

    @BindView(R.id.img_link)
    ImageView img_link;

    @BindView(R.id.txt_title_link)
    TextView txt_title_link;

    @BindView(R.id.txt_caption_link)
    TextView txt_caption_link;

    @BindView(R.id.txt_description_link)
    TextView txt_description_link;

    Context context;

    public LinkPreviewView(Context context) {
        super(context);
        this.context = context;
        initialize();
    }

    public LinkPreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initialize();
    }

    public LinkPreviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initialize();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LinkPreviewView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        initialize();
    }

    private void initialize(){
        View view = LayoutInflater.from(context).inflate(R.layout.widget_link_preview, (ViewGroup) getRootView(), true);
        ButterKnife.bind(this, view);
    }

    public void setPostLink(final Post post){
        Attachment attachment = post.attachments.data.get(0);

        // Load image of link
        if(attachment != null && attachment.media != null){
            Picasso.with(context)
                    .load(attachment.media.image.src)
                    .into(img_link);
        } else {
            img_link.setVisibility(View.GONE);
        }

        txt_title_link.setText(post.name);
        txt_caption_link.setText(post.caption);
        txt_description_link.setText(post.description);

        // Set link click action to open link
        link_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(post.link));
                context.startActivity(intent);
            }
        });
    }

    public void resetState(){
        link_container.setOnClickListener(null);
    }
}
