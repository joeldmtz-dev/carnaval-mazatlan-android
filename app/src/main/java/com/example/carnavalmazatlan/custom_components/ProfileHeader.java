package com.example.carnavalmazatlan.custom_components;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.carnavalmazatlan.R;
import com.github.kinnonii.timeago.TimeAgo;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by joeldmtz on 12/28/16.
 */

public class ProfileHeader extends LinearLayout {

    @BindView(R.id.img_profile)
    CircleImageView img_profile;

    @BindView(R.id.txt_profile_name)
    TextView txt_profile_name;

    @BindView(R.id.txt_timestamp)
    TextView txt_timestamp;

    @BindView(R.id.btn_open)
    ImageButton btn_open;

    Context context;

    public ProfileHeader(Context context) {
        super(context);
        initialize(context);
    }

    public ProfileHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public ProfileHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ProfileHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context);
    }

    public void initialize(Context context){
        this.context = context;
        View view = LayoutInflater.from(this.context).inflate(R.layout.widget_profile_header, (ViewGroup) getRootView(), true);
        ButterKnife.bind(this, view);
    }

    public void resetState(){
        btn_open.setVisibility(View.VISIBLE);
    }

    public void setHeaderInfo(String profileImgUrl, String profileName, Date postTime){
        setProfileInfo(profileImgUrl, profileName);
        setPostTime(postTime);
    }

    public void setProfileInfo(String profileImgUrl, String profileName){
        Glide.with(context)
                .load(profileImgUrl)
                .into(img_profile);

        txt_profile_name.setText(profileName);
    }

    public void setPostTime(Date postTime){
        TimeAgo time = new TimeAgo("es");
        txt_timestamp.setText(time.timeAgo(postTime));
    }

    public void setProvider(String provider){
        switch (provider){
            case "twitter":
                btn_open.setImageResource(R.drawable.ic_twitter_logo_silhouette);
                break;
            case "facebook":
                btn_open.setImageResource(R.drawable.ic_facebook);
                break;
            case "instagram":
                btn_open.setImageResource(R.drawable.ic_glyph_logo_may2016);
                break;
            case "youtube":
                btn_open.setImageResource(R.drawable.ic_youtube);
                break;
            default:
                btn_open.setVisibility(View.GONE);
                break;
        }
    }

    public ImageButton getButton(){
        return btn_open;
    }
}
