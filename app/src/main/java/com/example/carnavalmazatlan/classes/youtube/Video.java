package com.example.carnavalmazatlan.classes.youtube;

import java.util.Date;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class Video {
    public String etag;
    public String id;
    public String kind;
    public Snippet snippet;
}
