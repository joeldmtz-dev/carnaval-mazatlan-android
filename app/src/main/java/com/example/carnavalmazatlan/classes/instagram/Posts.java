package com.example.carnavalmazatlan.classes.instagram;

import java.util.List;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class Posts {
    public List<Post> data;
    public Meta meta;
    public Pagination pagination;

    public class Meta {
        public String code;
    }

    public class Pagination {

    }
}
