package com.example.carnavalmazatlan.classes.youtube;

import java.util.List;

/**
 * Created by joeldmtz on 12/29/16.
 */
public class Channel {
    public String etag;
    public List<Item> items;
    public String kind;

    public class Item {
        public String id;

        public String etag;

        public Snippet snippet;

        public String kind;
    }

    public class Snippet {
        public String publishedAt;

        public String title;

        public String description;

        public Thumbnails thumbnails;

        public String country;
    }

    public class Thumbnails {
        public Image high;
        public Image medium;
    }

    public class Image {
        public String url;
    }
}
