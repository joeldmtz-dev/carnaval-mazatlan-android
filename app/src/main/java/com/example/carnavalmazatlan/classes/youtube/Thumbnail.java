package com.example.carnavalmazatlan.classes.youtube;

/**
 * Created by joeldmtz on 12/29/16.
 */

public class Thumbnail {
    public Image high;
    public Image maxres;
    public Image medium;
    public Image standard;
}
