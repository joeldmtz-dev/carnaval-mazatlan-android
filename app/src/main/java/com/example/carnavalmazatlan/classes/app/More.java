package com.example.carnavalmazatlan.classes.app;

public class More {
    private String img;
    private String title;

    private String subtitle;
    public More(String title, String subtitle){
        this.title = title;
        this.subtitle = subtitle;
    }

    public String getImg() {
        return img;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
