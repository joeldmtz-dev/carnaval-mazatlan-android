package com.example.carnavalmazatlan.classes.app;

import com.orm.SugarRecord;

public class Notification extends SugarRecord{

    private int notificationReminder;
    private int days;

    public Notification(){}

    public int getNotificationReminder() {
        return notificationReminder;
    }

    public void setNotificationReminder(int notificationReminder) {
        this.notificationReminder = notificationReminder;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}