package com.example.carnavalmazatlan.classes.app;

import java.io.Serializable;

/**
 * Created by joeldmtz on 1/7/17.
 */

public class Place implements Serializable {
    private String name;

    public Place(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
