package com.example.carnavalmazatlan.classes.app;

/**
 * Created by joeldmtz on 12/31/16.
 */

public class Candidate {
    private boolean selected;
    private int id;
    private String name;

    public Candidate() {
        this.selected =false;
    }

    public Candidate(int id, String name) {
        this.id = id;
        this.name = name;
        this.selected =false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selection) {
        this.selected = selection;
    }
}
