package com.example.carnavalmazatlan.classes.facebook;

import java.util.Date;
import java.util.List;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class Post {
    public String id;
    public String message;
    public String type;
    public String source;
    public String link;
    public String name;
    public String caption;
    public String description;
    public Date createdTime;
    public Shares shares;
    public Evelope attachments;

    public class Evelope {
        public List<Attachment> data;
    }

    public class Shares {
        public int count;
    }
}