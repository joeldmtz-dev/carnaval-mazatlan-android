package com.example.carnavalmazatlan.classes.instagram;

import java.util.Date;
import java.util.List;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class Post {

    public Commnet comments;
    public Caption caption;
    public Likes likes;
    public String link;
    public User user;
    public Date createdTime;
    public Images images;
    public Videos videos;
    public String type;
    public List<Object> usersInPhoto;
    public String filteR;
    public List<String> tags;
    public String id;
    public Location location;


    public class Commnet {
        public int count;
    }

    public class Caption {
        public Date createdTime;
        public String text;
        public From from;
        public String id;

    }

    public class From {
        public String username;
        public String fullName;
        public String type;
        public String id;
    }

    public class Likes {
        public int count;
    }

    public class User {
        public String username;
        public String profilePicture;
        public String id;
    }

    public class Images {
        public Media lowResolution;
        public Media thumbnail;
        public Media standardResolution;
    }

    public class Videos {
        public Media lowResolution;
        public Media standardResolution;
    }

    public class Media {
        public String url;
        public int width;
        public int height;
    }

    public class Location {
        public double latitude;
        public double longitude;
        public String id;
        public String streetAddress;
        public String name;
    }

}
