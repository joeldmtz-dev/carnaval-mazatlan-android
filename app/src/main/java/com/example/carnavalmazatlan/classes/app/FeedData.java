package com.example.carnavalmazatlan.classes.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class FeedData {
    public List<FeedItem> data;
    public Pagination pagination;

    public static class Pagination {
        public String nextUntilDate;

        public static Pagination newEmptyInstance(){
            return new Pagination();
        }
    }

    public static FeedData newEmptyInstance(){
        FeedData feed = new FeedData();
        feed.data = new ArrayList<>();
        feed.pagination = Pagination.newEmptyInstance();
        return feed;
    }
}
