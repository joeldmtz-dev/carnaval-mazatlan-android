package com.example.carnavalmazatlan.classes.app;

import com.example.carnavalmazatlan.classes.youtube.Video;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.Date;
import java.util.List;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class FeedItem {
    public String provider;
    public Date date;
    public Tweet dataTwitter;
    public com.example.carnavalmazatlan.classes.facebook.Post dataFacebook;
    public com.example.carnavalmazatlan.classes.instagram.Post dataInstagram;
    public Video dataYoutube;
    public List<String> headerItem;
}
