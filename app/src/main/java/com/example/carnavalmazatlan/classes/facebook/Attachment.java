package com.example.carnavalmazatlan.classes.facebook;

import java.util.List;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class Attachment {
    public String title;
    public String type;
    public String description;
    public Media media;

    public Evelope subattachments;

    public class Evelope {
        public List<Subattacment> data;
    }

    public class Media {
        public Image image;
    }

    public class Image {
        public int height;
        public int width;
        public String src;
    }

    public class Subattacment {
        public String title;
        public String type;
        public String description;
        public Media media;
    }

}