package com.example.carnavalmazatlan.classes.youtube;

import java.util.Date;

/**
 * Created by joeldmtz on 12/29/16.
 */

public class Snippet {
    public String channelId;
    public String channelTitle;
    public String title;
    public String description;
    public String playlistId;
    public int position;
    public Date publishedAt;
    public ResourceId resourceId;
    public Thumbnail thumbnails;
}
