package com.example.carnavalmazatlan.classes.app;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class Events extends SugarRecord implements Parcelable{

    private String eventName;
    private String eventTime;
    private String eventRemainingTime;
    private String eventLocation;
    private String eventDate;

    public Events(){}

    public Events(String eventName, String eventTime, String eventRemainingTime, String eventLocation,
                  String eventDate){
        this.eventName = eventName;
        this.eventTime = eventTime;
        this.eventRemainingTime = eventRemainingTime;
        this.eventLocation = eventLocation;
        this.eventDate = eventDate;
    }

    public String getEventLocation() {
        return eventLocation;
    }
    public String getEventName() {
        return eventName;
    }
    public String getEventTime() {
        return eventTime;
    }
    public String getEventRemainingTime() {
        return eventRemainingTime;
    }
    public String getEventDate() {
        return eventDate;
    }

    public Events(Parcel in){
        String[] data = new String[5];

        in.readStringArray(data);
        this.eventName = data[0];
        this.eventTime = data[1];
        this.eventRemainingTime = data[2];
        this.eventLocation = data[3];
        this.eventDate = data[4];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{this.eventName, this.eventTime, this.eventRemainingTime,
                this.eventLocation, this.eventDate});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Events createFromParcel(Parcel in) {
            return new Events(in);
        }

        public Events[] newArray(int size) {
            return new Events[size];
        }
    };
}
