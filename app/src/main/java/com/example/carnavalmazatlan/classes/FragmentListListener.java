package com.example.carnavalmazatlan.classes;

/**
 * Created by joeldmtz on 12/29/16.
 */

public interface FragmentListListener {
    void onListScrolled();
}
