package com.example.carnavalmazatlan.classes.facebook;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class Posts {
    public List<Post> data;
    public Posts.Paging paging;

    public class Paging {
        public String previous;
        public String next;
    }

    public static Posts newEmptyInstance(){
        Posts posts = new Posts();
        posts.data = new ArrayList<Post>();

        return posts;
    }
}
