package com.example.carnavalmazatlan.socialfeeds;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.FacebookPostAdapter;
import com.example.carnavalmazatlan.apis.Callback;
import com.example.carnavalmazatlan.apis.FacebookApi;
import com.example.carnavalmazatlan.classes.facebook.Page;
import com.example.carnavalmazatlan.classes.facebook.Post;
import com.example.carnavalmazatlan.classes.facebook.Posts;
import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/14/16.
 */

public class FacebookFeedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeLayout;

    private FacebookPostAdapter adapter;
    private boolean isLoading = false;

    public FacebookFeedFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facebook_feed, container, false);

        ButterKnife.bind(this, view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter = new FacebookPostAdapter(Posts.newEmptyInstance(), new Page(), getActivity());
        recyclerView.setAdapter(adapter);

        initialLoad();
        setInfiniteScroll();

        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.colorAccent);

        return view;
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(true);

        FacebookApi.getPosts(new Callback(){
            @Override
            public void success(Posts posts) {
                adapter.setData(posts);
                swipeLayout.setRefreshing(false);
            }
        });
    }

    public void initialLoad(){
        FacebookApi.getPageProfile(new Callback(){
            @Override
            public void success(Page page) {
                adapter.setProfile_page(page);

                FacebookApi.getPosts(new Callback() {
                    @Override
                    public void success(Posts posts) {
                        adapter.addMore(posts);
                    }
                });
            }
        });
    }

    public void setInfiniteScroll(){
        BaseAttacher attacher = Mugen.with(recyclerView, new MugenCallbacks() {
            @Override
            public void onLoadMore() {
                loadMore();
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        }).start();

        /* Use this to dynamically turn infinite scroll on or off. It is enabled by default */
        attacher.setLoadMoreEnabled(true);

        /* Use this to change when the onLoadMore() function is called.
        * By default, it is called when the scroll reaches 2 items from the bottom */
        attacher.setLoadMoreOffset(4);
    }

    public void loadMore(){
        if (adapter.getNextPage() != null){
            isLoading = true;
            FacebookApi.getPosts(adapter.getNextPage(), new Callback(){
                @Override
                public void success(Posts posts) {
                    adapter.addMore(posts);
                    isLoading = false;
                }
            });
        }
    }
}
