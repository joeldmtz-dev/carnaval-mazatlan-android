package com.example.carnavalmazatlan.socialfeeds;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.apis.ApiFactory;
import com.example.carnavalmazatlan.apis.InstagramApiService;
import com.example.carnavalmazatlan.classes.instagram.Posts;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class InstagramFeedFragment extends Fragment {

    public InstagramFeedFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_instagram_feed, container, false);

        Call<Posts> call = ApiFactory
                .newInstagramInstance()
                .getPosts("36646503");

        call.enqueue(new Callback<Posts>() {
            @Override
            public void onResponse(Call<Posts> call, Response<Posts> response) {
                if(response.code() == 200){
                    System.out.println(">>>>>" + response.body().data.size());
                }

                System.out.println("*******"+response.raw().toString());
            }

            @Override
            public void onFailure(Call<Posts> call, Throwable t) {
                t.printStackTrace();
            }
        });

        return view;
    }
}
