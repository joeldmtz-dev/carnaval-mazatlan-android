package com.example.carnavalmazatlan.apis;

import android.content.Context;

import com.example.carnavalmazatlan.classes.youtube.ResourceId;
import com.example.carnavalmazatlan.classes.youtube.Snippet;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class ApiFactory {

    public static final int MAX_REQUESTS = 5;

    public static InstagramApiService newInstagramInstance(){
        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong()  * 1000);
            }
        });
        Gson gson = gsonBuilder.create();

        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("access_token", InstagramApiService.DEFAULT_ACCESSTOKEN)
                        .build();

                Request request = original.newBuilder().url(url).build();

                return chain.proceed(request);
            }
        }).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(InstagramApiService.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson));

        return builder
                .client(httpClient)
                .build()
                .create(InstagramApiService.class);
    }

    public static CarnavalMazatlanApiService newCarnavalMazatlanInstance(){
        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                String formatA = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS";
                String formatB = "yyyy-MM-dd'T'HH:mm:ss+SSSSSSS";

                try {
                    return new SimpleDateFormat(formatA, Locale.ENGLISH)
                            .parse(json.getAsString());
                } catch (ParseException e) {
                    try {
                        return new SimpleDateFormat(formatB, Locale.ENGLISH)
                                .parse(json.getAsString());
                    } catch (ParseException ex) {
                        return new Date(json.getAsJsonPrimitive().getAsLong() * 1000);
                    }
                }
            }
        });

        gsonBuilder.registerTypeAdapter(Snippet.class, new JsonDeserializer<Snippet>() {
            @Override
            public Snippet deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                Gson gson = new Gson();
                Snippet snippet =  gson.fromJson(json, Snippet.class);
                return snippet;
            }
        });

        Gson gson = gsonBuilder
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(CarnavalMazatlanApiService.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson));

        return builder
                .build()
                .create(CarnavalMazatlanApiService.class);
    }

    public static YoutubeApiService newYoutubeInstance(){
        Gson gson = new GsonBuilder().create();

        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("key", YoutubeApiService.API_KEY)
                        .build();

                Request request = original.newBuilder().url(url).build();

                return chain.proceed(request);
            }
        }).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(YoutubeApiService.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson));

        return builder
                .client(httpClient)
                .build()
                .create(YoutubeApiService.class);
    }

}
