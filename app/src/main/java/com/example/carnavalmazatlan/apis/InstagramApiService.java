package com.example.carnavalmazatlan.apis;

import com.example.carnavalmazatlan.classes.instagram.Posts;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by joeldmtz on 12/17/16.
 */

public interface InstagramApiService {

    String ENDPOINT = "https://api.instagram.com/v1/";
    String DEFAULT_ACCESSTOKEN = "4280248053.9a42bc3.7bd64c31ff314c638cdde62292e9300c";
    String DEFAULT_PAGEID = "self";

    @GET("users/{user_id}/media/recent")
    Call<Posts> getPosts(@Path("user_id") String user_id);

}
