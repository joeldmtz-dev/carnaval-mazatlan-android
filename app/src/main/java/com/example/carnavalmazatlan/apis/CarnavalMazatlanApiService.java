package com.example.carnavalmazatlan.apis;

import com.example.carnavalmazatlan.classes.app.FeedData;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by joeldmtz on 12/26/16.
 */

public interface CarnavalMazatlanApiService {

    String ENDPOINT = "http://162.243.131.189:3000/";

    @GET("feed")
    Call<FeedData> getFeed(@QueryMap Map<String, String> options);

    @GET("feed/people")
    Call<FeedData> getFeedSocial(@QueryMap Map<String, String> options);

}
