package com.example.carnavalmazatlan.apis;

import com.example.carnavalmazatlan.classes.facebook.Page;
import com.example.carnavalmazatlan.classes.facebook.Posts;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by joeldmtz on 12/17/16.
 */

public class FacebookApi {
    public static final String FACEBOOK_URL = "https://www.facebook.com/";
    public static final String FACEBOOK_PAGE_ID = "CarnavalInternacionalMazatlan";

    public static final String PAGE_ID = "119696371435885";
    public  static final String POSTS_ENDPOINT = PAGE_ID + "/posts?fields=message,attachments{media,title,type,subattachments,description},type,source,link,name,caption,description,created_time,likes.limit(0),comments.limit(0),shares";
    public static final String PAGEPROFILE_ENDPOINT = PAGE_ID + "/?fields=picture{url},name";


    public static void getPosts(final Callback callback){
        getPosts(POSTS_ENDPOINT, callback);
    }

    public static void getPosts(String endpoint, final Callback callback){
        GraphRequest request = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(),
                endpoint.replace("https://graph.facebook.com/v2.8/", ""),
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        Gson gson = new GsonBuilder()
                                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                                .create();
                        try {
                            Posts posts = gson.fromJson(response.getJSONObject().toString(), Posts.class);
                            callback.success(posts);
                        } catch (NullPointerException e) {
                            System.out.println(response.getError());
                        }
                    }
                });
        request.executeAsync();
    }

    public static void getPageProfile(final Callback callback){
        getPageProfile(PAGEPROFILE_ENDPOINT, callback);
    }

    public static void getPageProfile(String endpoint, final Callback callback){
        GraphRequest request = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(),
                endpoint.replace("https://graph.facebook.com/v2.8/", ""),
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        Gson gson = new GsonBuilder()
                                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                                .create();
                        try {
                            Page page = gson.fromJson(response.getJSONObject().toString(), Page.class);
                            callback.success(page);
                        } catch (NullPointerException e) {
                            System.out.println(response.getError());
                        }
                    }
                });
        request.executeAsync();
    }
}
