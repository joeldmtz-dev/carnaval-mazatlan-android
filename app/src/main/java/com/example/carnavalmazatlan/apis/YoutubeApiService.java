package com.example.carnavalmazatlan.apis;

import com.example.carnavalmazatlan.classes.youtube.Channel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by joeldmtz on 12/29/16.
 */

public interface YoutubeApiService {

    String API_KEY = "AIzaSyDnPXW8_whZjQzCTk08eOQEu8Znz8LcCRs";
    String ENDPOINT = "https://content.googleapis.com/youtube/v3/";

    @GET("channels")
    Call<Channel> getChannelProfile(@Query("forUsername") String username, @Query("part") String part);

}
