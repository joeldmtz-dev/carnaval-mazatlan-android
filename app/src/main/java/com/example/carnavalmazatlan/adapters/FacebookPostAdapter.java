package com.example.carnavalmazatlan.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.FacebookPostHolder;
import com.example.carnavalmazatlan.classes.facebook.Page;
import com.example.carnavalmazatlan.classes.facebook.Post;
import com.example.carnavalmazatlan.classes.facebook.Posts;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.example.carnavalmazatlan.custom_components.VideoPreview;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by joeldmtz on 12/17/16.
 */

public class FacebookPostAdapter extends RecyclerView.Adapter<FacebookPostHolder> implements VideoLoadListener{

    private List<Post> mDataset = new ArrayList<>();
    private String nextPage;
    private Page profile_page;
    private Context context;

    private SimpleExoPlayer player;
    private DataSource.Factory dataSourceFactory;
    private ExtractorsFactory extractorsFactory;

    private VideoPreview nowPlaying = null;

    public FacebookPostAdapter(Posts posts, Page profile_page, Context context) {
        addMore(posts);

        this.profile_page = profile_page;
        this.context = context;

        createVideoPlayer();
    }

    @Override
    public FacebookPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_card_facebook_post, parent, false);

        FacebookPostHolder holder = new FacebookPostHolder(view);
        holder.setVideoPlayer(player, dataSourceFactory, extractorsFactory, this);

        return holder;
    }

    @Override
    public void onBindViewHolder(final FacebookPostHolder holder, int position) {
        holder.clean();

        final Post post = mDataset.get(position);
        holder.setProfile_info(profile_page);
        holder.init(context, post);
    }

    private void createVideoPlayer(){
        // Create video player to use in the posts

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        // Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();

        // Create the player
        player = ExoPlayerFactory.newSimpleInstance(this.context, trackSelector, loadControl);

        DefaultBandwidthMeter bandwidthMeter2 = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        dataSourceFactory = new DefaultDataSourceFactory(this.context,
                Util.getUserAgent(this.context, "yourApplicationName"), bandwidthMeter2);

        // Produces Extractor instances for parsing the media data.
        extractorsFactory = new DefaultExtractorsFactory();
    }

    @Override
    public void onLoadVideo(View videoPreview) {
        VideoPreview nextPlaying = (VideoPreview) videoPreview;

        if (nowPlaying != null){
            // Delete player for the post
            nowPlaying.resetState();
        }

        nowPlaying = nextPlaying;
        nextPlaying.setPlayerView(player);
    }

    @Override
    public void onPlayVideo() {
        nowPlaying.showVideo();
    }

    public void setData(Posts posts){
        mDataset = posts.data;
        notifyDataSetChanged();
        if(posts.paging != null && posts.paging.next != null && !posts.paging.next.equals("")){
            nextPage = posts.paging.next;
        } else {
            nextPage = null;
        }
    }

    public void addMore(Posts posts){
        int previousLastPosition = mDataset.size();
        mDataset.addAll(posts.data);
        notifyItemRangeInserted(previousLastPosition, posts.data.size());

        if(posts.paging != null && posts.paging.next != null && !posts.paging.next.equals("")){
            nextPage = posts.paging.next;
        } else {
            nextPage = null;
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public List<Post> getmDataset() {
        return mDataset;
    }

    public Page getProfile_page() {
        return profile_page;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setProfile_page(Page profile_page) {
        this.profile_page = profile_page;
    }
}
