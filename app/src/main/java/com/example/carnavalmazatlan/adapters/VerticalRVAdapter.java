package com.example.carnavalmazatlan.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.EmptyHolder;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.FacebookPostHolder;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.FeedHeaderHolder;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.FeedViewHolder;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.InstagramPostHolder;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.TwitterTweetHolder;
import com.example.carnavalmazatlan.adapters.socialfeed_viewholders.YoutubeVideoHolder;
import com.example.carnavalmazatlan.classes.app.FeedData;
import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.classes.facebook.Page;
import com.example.carnavalmazatlan.classes.youtube.Channel;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.example.carnavalmazatlan.custom_components.VideoPreview;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

public class VerticalRVAdapter extends RecyclerView.Adapter<FeedViewHolder> implements VideoLoadListener{

    private final int HEADER = 0, TWITTER = 1, FACEBOOK = 2, INSTAGRAM = 3, YOUTUBE = 4;

    private final Context context;

    private FeedItem headerItem = null;
    private List<FeedItem> mDataset;
    private String nextUntilDate = null;

    private SimpleExoPlayer player;
    private DataSource.Factory dataSourceFactory;
    private ExtractorsFactory extractorsFactory;

    private VideoPreview nowPlaying = null;

    private Page defaultFacebookProfileInfo = null;
    private Channel defaultYoutubeChannelInfo = null;

    public VerticalRVAdapter(Context context, FeedData feed) {
        this.context = context;
        setData(feed);
        if (feed.data.size() > 0) {
            headerItem = feed.data.get(0);
        }

        createVideoPlayer();
    }

    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FeedViewHolder holder;
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);

        switch (viewType) {
            case HEADER:
                view = inflater.inflate(R.layout.item_header_feed_events, parent, false);
                holder = new FeedHeaderHolder(view);
                break;
            case TWITTER:
                view = inflater.inflate(R.layout.item_card_twitter_tweet, parent, false);
                holder = new TwitterTweetHolder(view);
                break;
            case FACEBOOK:
                view = inflater.inflate(R.layout.item_card_facebook_post, parent, false);
                holder = new FacebookPostHolder(view, defaultFacebookProfileInfo);
                holder.setVideoPlayer(player, dataSourceFactory, extractorsFactory, this);
                break;
            case INSTAGRAM:
                view = inflater.inflate(R.layout.item_card_instagram_post, parent, false);
                holder = new InstagramPostHolder(view);
                break;
            case YOUTUBE:
                view = inflater.inflate(R.layout.item_card_youtube_video, parent, false);
                holder = new YoutubeVideoHolder(view, defaultYoutubeChannelInfo);
                break;
            default:
                view = inflater.inflate(R.layout.empty_layout, parent, false);
                holder = new EmptyHolder(view);
                break;
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, final int position) {
        holder.clean();
        holder.init(context, mDataset.get(position));
    }

    private void createVideoPlayer(){
        // Create video player to use in the posts

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        // Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();

        // Create the player
        player = ExoPlayerFactory.newSimpleInstance(this.context, trackSelector, loadControl);

        DefaultBandwidthMeter bandwidthMeter2 = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        dataSourceFactory = new DefaultDataSourceFactory(this.context,
                Util.getUserAgent(this.context, "yourApplicationName"), bandwidthMeter2);

        // Produces Extractor instances for parsing the media data.
        extractorsFactory = new DefaultExtractorsFactory();
    }

    @Override
    public void onLoadVideo(View videoPreview) {
        VideoPreview nextPlaying = (VideoPreview) videoPreview;

        if (nowPlaying != null){
            // Delete player for the post
            nowPlaying.resetState();
        }

        nowPlaying = nextPlaying;
        nextPlaying.setPlayerView(player);
    }

    @Override
    public void onPlayVideo() {
        nowPlaying.showVideo();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getItemViewType(int position) {
        int type = -1;

        switch (mDataset.get(position).provider){
            case "header":
                type = HEADER;
                break;
            case "twitter":
                type = TWITTER;
                break;
            case "facebook":
                type = FACEBOOK;
                break;
            case "instagram":
                type = INSTAGRAM;
                break;
            case "youtube":
                type = YOUTUBE;
                break;
        }

        return type;
    }

    public void setData(FeedData feed){
        this.mDataset = new ArrayList<>();
        if (headerItem != null) {
            this.mDataset.add(headerItem);
        }
        this.mDataset.addAll(feed.data);

        if (feed.pagination != null && feed.pagination.nextUntilDate != null && !feed.pagination.nextUntilDate.equals("")) {
            this.nextUntilDate = feed.pagination.nextUntilDate;
        }
        notifyDataSetChanged();
    }

    public void setDefaultFacebookProfileInfo(Page defaultFacebookProfileInfo) {
        this.defaultFacebookProfileInfo = defaultFacebookProfileInfo;
    }

    public void setDefaultYoutubeChannelInfo(Channel defaultYoutubeChannelInfo) {
        this.defaultYoutubeChannelInfo = defaultYoutubeChannelInfo;
    }

    public List<FeedItem> getmDataset() {
        return mDataset;
    }

    public void setHeaderItem(FeedItem headerItem) {
        this.headerItem = headerItem;
    }

    public String getNextUntilDate() {
        return nextUntilDate;
    }

    public void addMore(FeedData feed){
        int previousLastPosition = mDataset.size();
        mDataset.addAll(feed.data);
        notifyItemRangeInserted(previousLastPosition, feed.data.size());

        if (feed.pagination != null && feed.pagination.nextUntilDate != null && !feed.pagination.nextUntilDate.equals("")) {
            this.nextUntilDate = feed.pagination.nextUntilDate;
        }
    }
}
