package com.example.carnavalmazatlan.adapters.socialfeed_viewholders;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.apis.FacebookApi;
import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.classes.facebook.Attachment;
import com.example.carnavalmazatlan.classes.facebook.Page;
import com.example.carnavalmazatlan.classes.facebook.Post;
import com.example.carnavalmazatlan.custom_components.ExpandableTextView;
import com.example.carnavalmazatlan.custom_components.LinkPreviewView;
import com.example.carnavalmazatlan.custom_components.ProfileHeader;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.example.carnavalmazatlan.custom_components.VideoPreview;
import com.github.kinnonii.timeago.TimeAgo;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.upstream.DataSource;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/26/16.
 */
public class FacebookPostHolder extends FeedViewHolder {

    private Page profile_info = null;

    // Header components
    @BindView(R.id.facebook_profile_header)
    ProfileHeader profile_header;

    // Status components
    @BindView(R.id.expandable_text)
    ExpandableTextView expandableTextView;

    // Media components
    @BindView(R.id.img_attachment)
    ImageView img_attachment;

    @BindView(R.id.video_attachment_container)
    VideoPreview video_attachment;

    // Link components
    @BindView(R.id.link_preview)
    LinkPreviewView link_preview;

    public FacebookPostHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public FacebookPostHolder(View itemView, Page defaultProfileInfo) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.profile_info = defaultProfileInfo;
    }

    @Override
    public void clean() {
        img_attachment.setVisibility(View.VISIBLE);
        video_attachment.setVisibility(View.VISIBLE);
        link_preview.setVisibility(View.VISIBLE);

        img_attachment.setOnClickListener(null);
        video_attachment.resetState();
        link_preview.resetState();

        profile_header.resetState();
    }

    @Override
    public void init(Context context, FeedItem item) {
        init(context, item.dataFacebook);
    }

    public void init(Context context, Post post) {
        initProfileHeader(context, post);
        initStatus(context, post);
        initMedia(context, post);
        initActions(context, post);

        // Refresh ExpandableTextView to collapse and reset the state
        expandableTextView.refreshState();
    }

    public void setProfile_info(Page profile_info) {
        this.profile_info = profile_info;
    }

    public void setVideoPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory, VideoLoadListener listener){
        video_attachment.setUpPlayer(player, dataSourceFactory, extractorsFactory);
        video_attachment.setVideLoadlistener(listener);
    }

    private void  initProfileHeader(Context context, Post post){
        if (profile_info != null) {
            profile_header.setProfileInfo(profile_info.picture.data.url, profile_info.name);
        }

        profile_header.setPostTime(post.createdTime);
        profile_header.setProvider("facebook");
    }

    private void initStatus(final Context context, Post post){
        // Set status message in ExpandableTextView
        expandableTextView.setText(post.message);

        // Get TextView inside ExpandableTextView
        TextView txt_status_message = expandableTextView.getTextView();

        // HashTagHelper to handle hastags in the status message
        HashTagHelper mTextHashTagHelper = HashTagHelper.Creator.create(context.getResources().getColor(R.color.colorAccent), new HashTagHelper.OnHashTagClickListener() {
            @Override
            public void onHashTagClicked(String hashTag) {
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String facebookUrl = getFacebookHashTagURL(context, hashTag);
                facebookIntent.setData(Uri.parse(facebookUrl));
                context.startActivity(facebookIntent);
            }
        });

        // Set the hashtag helper to the status message TextView
        mTextHashTagHelper.handle(txt_status_message);

        // Add links to status message TextView
        Linkify.addLinks(txt_status_message, Linkify.WEB_URLS);

        // Set custom color for links in status message TextView
        //txt_status_message.setLinkTextColor();
    }

    private String getMediaType(Post post){
        if (post.type.equals("photo") && post.attachments != null && post.attachments.data != null) {
            return "photo";
        } else if (post.type.equals("video") && post.source != null) {
            if (post.source.contains("https://www.youtube.com/embed/")){
                return "link";
            }
            return "video";
        } else if (post.type.equals("link")
                && post.link != null
                && post.name != null
                && post.caption != null
                && post.description != null){
            return "link";
        }

        return "status";
    }

    private void initMedia(Context context, final Post post){
        //Get first Attachment
        Attachment attachment = post.attachments.data.get(0);

        // Check attachment type
        switch (getMediaType(post)) {
            case "photo":
                loadImage(context, attachment);
                break;
            case "video":
                loadVideo(post);
                break;
            case "link":
                loadLink(post);
                break;
            case "status":
                hideAll();
                break;
            default:
                hideAll();
                break;
        }
    }

    private void initActions(final Context context, final Post post){
        profile_header.getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String facebookUrl = getFacebookPageURL(context, post.type, post.id, post.source);
                facebookIntent.setData(Uri.parse(facebookUrl));
                context.startActivity(facebookIntent);
            }
        });
    }

    private void hideAll(){
        img_attachment.setVisibility(View.GONE);
        video_attachment.setVisibility(View.GONE);
        video_attachment.setVisibility(View.GONE);
    }

    private void loadImage(Context context, Attachment attachment){
        // Check if attachment and media are available
        if(attachment != null && attachment.media != null){
            Glide.with(context)
                    .load(attachment.media.image.src)
                    .into(img_attachment);
        } else {
            img_attachment.setVisibility(View.GONE);
        }

        video_attachment.setVisibility(View.GONE);
        link_preview.setVisibility(View.GONE);
    }

    private void loadVideo(Post post){
        video_attachment.setMediaContent(post);

        img_attachment.setVisibility(View.GONE);
        link_preview.setVisibility(View.GONE);
    }

    private void loadLink(final Post post){
        link_preview.setPostLink(post);

        img_attachment.setVisibility(View.GONE);
        video_attachment.setVisibility(View.GONE);
    }

    private String getFacebookPageURL(Context context, String type, String id, String source) {
        String newUrl, oldUrl, standard_url;
        String ID = id.replace(FacebookApi.PAGE_ID+"_", "");
        String basUrl = FacebookApi.FACEBOOK_URL + FacebookApi.FACEBOOK_PAGE_ID;

        if (type.equals("video") && source != null && !source.contains("https://www.youtube.com/embed/")) {
            newUrl = "fb://facewebmodal/f?href=" + basUrl + "/videos/" + ID;
            oldUrl = "fb://videos/" + id;
            standard_url = basUrl + "/videos/" + ID;
        } else {
            newUrl = "fb://facewebmodal/f?href=" + basUrl + "/posts/" + ID;
            oldUrl = "fb://posts/" + id;
            standard_url = basUrl + "/posts/" + ID;
        }

        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return newUrl;
            } else { //older versions of fb app
                return oldUrl;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return standard_url;
        }
    }

    private String getFacebookHashTagURL(Context context, String hashtag){
        String newUrl, oldUrl, standard_url;
        String tag = hashtag.replace("#", "");

        newUrl = "fb://facewebmodal/f?href=" + FacebookApi.FACEBOOK_URL + "/hashtag/" + tag;
        oldUrl = "fb://hashtag/" + tag;
        standard_url = FacebookApi.FACEBOOK_URL + "/videos/" + tag;

        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return newUrl;
            } else { //older versions of fb app
                return oldUrl;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return standard_url;
        }
    }
}
