package com.example.carnavalmazatlan.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.activities.EventActivity;
import com.example.carnavalmazatlan.activities.TicketActivity;
import com.example.carnavalmazatlan.classes.app.Place;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 1/7/17.
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.TicketHolder> {

    private List<Place> mDataset;
    private Context context;

    class TicketHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.rlPlaceInfo)
        RelativeLayout placeInfo;

        @BindView(R.id.tvPlaceName)
        TextView placeName;


        public TicketHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent intent = new Intent(v.getContext(), TicketActivity.class);
                    intent.putExtra("event", mDataset.get(getAdapterPosition()));
                    intent.putExtra("position", getAdapterPosition());
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    public TicketAdapter(List<Place> mDataset, Context context) {
        this.mDataset = mDataset;
        this.context = context;
    }

    @Override
    public TicketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_place, parent, false);
        return new TicketHolder(view);
    }

    @Override
    public void onBindViewHolder(TicketHolder holder, int position) {
        holder.placeName.setText(mDataset.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
