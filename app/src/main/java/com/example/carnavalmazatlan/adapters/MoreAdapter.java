package com.example.carnavalmazatlan.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.app.More;

import java.util.ArrayList;

public class MoreAdapter extends ArrayAdapter<More>{
    private Context ctx;
    private int layoutResourceId;
    private ArrayList<More> data;

    public MoreAdapter(Context ctx, int layoutResourceId, ArrayList<More> data){
        super(ctx, layoutResourceId, data);
        this.ctx = ctx;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        MoreHolder moreHolder;
        if(view == null){
            LayoutInflater inflater = ((Activity) ctx).getLayoutInflater();
            view = inflater.inflate(layoutResourceId, parent, false);
            moreHolder = new MoreHolder();
            moreHolder.menu_title = (TextView) view.findViewById(R.id.menu_title);
            moreHolder.menu_subtitle = (TextView) view.findViewById(R.id.menu_subtitle);
            view.setTag(moreHolder);
        }else{
            moreHolder = (MoreHolder) view.getTag();
        }
        moreHolder.menu_title.setText(data.get(position).getTitle());
        moreHolder.menu_subtitle.setText(data.get(position).getSubtitle());
        return view;
    }

    private class MoreHolder{
        ImageView menu_img;
        TextView menu_title, menu_subtitle;
    }
}
