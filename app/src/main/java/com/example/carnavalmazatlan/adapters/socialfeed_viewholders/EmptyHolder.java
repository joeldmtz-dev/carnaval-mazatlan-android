package com.example.carnavalmazatlan.adapters.socialfeed_viewholders;

import android.content.Context;
import android.view.View;

import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.upstream.DataSource;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class EmptyHolder extends FeedViewHolder {
    public EmptyHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void clean() {

    }

    @Override
    public void init(Context context, FeedItem item) {

    }

    @Override
    public void setVideoPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory, VideoLoadListener listener) {

    }
}
