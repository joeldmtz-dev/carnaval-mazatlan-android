package com.example.carnavalmazatlan.adapters.socialfeed_viewholders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.classes.facebook.Post;
import com.example.carnavalmazatlan.custom_components.LinkPreviewView;
import com.example.carnavalmazatlan.custom_components.ProfileHeader;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.example.carnavalmazatlan.custom_components.VideoPreview;
import com.github.kinnonii.timeago.TimeAgo;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.upstream.DataSource;
import com.twitter.sdk.android.core.models.MediaEntity;
import com.twitter.sdk.android.core.models.Tweet;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class TwitterTweetHolder extends FeedViewHolder {

    @BindView(R.id.twitter_profile_header)
    ProfileHeader profile_header;

    @BindView(R.id.twitter_txt_status_message)
    TextView txt_status_message;

    @BindView(R.id.twitter_img_attachment)
    ImageView img_attachment;

    @BindView(R.id.twitter_video_attachment_container)
    VideoPreview video_attachment;

    @BindView(R.id.twitter_link_preview)
    LinkPreviewView link_preview;

    public TwitterTweetHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void clean() {
        img_attachment.setVisibility(View.VISIBLE);
        video_attachment.setVisibility(View.VISIBLE);
        link_preview.setVisibility(View.VISIBLE);

        img_attachment.setOnClickListener(null);
        video_attachment.resetState();
        link_preview.resetState();

        profile_header.resetState();
    }

    @Override
    public void init(Context context, FeedItem item) {
        initProfileHeader(context, item.dataTwitter);
        initStatus(context, item.dataTwitter);
        initMedia(context, item.dataTwitter);
    }

    private void  initProfileHeader(final Context context, final Tweet tweet){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
            formatter.setLenient(true);
            Date date = formatter.parse(tweet.createdAt);
            profile_header.setHeaderInfo(tweet.user.profileImageUrl, tweet.user.name, date);
        } catch (ParseException e) {
            Date date = new Date();
            profile_header.setHeaderInfo(tweet.user.profileImageUrl, tweet.user.name, date);
        }

        profile_header.setProvider("twitter");
        profile_header.getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/"+tweet.user.screenName+"/status/"+tweet.idStr));
                context.startActivity(intent);
            }
        });
    }

    private void initStatus(final Context context, Tweet tweet){
        // Set status message in ExpandableTextView
        txt_status_message.setText(tweet.text);

        // HashTagHelper to handle hastags in the status message
        HashTagHelper mTextHashTagHelper = HashTagHelper.Creator.create(context.getResources().getColor(R.color.colorAccent), new HashTagHelper.OnHashTagClickListener() {
            @Override
            public void onHashTagClicked(String hashTag) {

            }
        });

        // Set the hashtag helper to the status message TextView
        mTextHashTagHelper.handle(txt_status_message);

        // Add links to status message TextView
        Linkify.addLinks(txt_status_message, Linkify.WEB_URLS);

        // Set custom color for links in status message TextView
        //txt_status_message.setLinkTextColor();
    }

    private void initMedia(final Context context, Tweet tweet){
        // Check attachment type
        switch (getMediaType(tweet)) {
            case "photo":
                Glide.with(context)
                        .load(tweet.entities.media.get(0).mediaUrl)
                        .into(img_attachment);

                video_attachment.setVisibility(View.GONE);
                link_preview.setVisibility(View.GONE);
                break;
            case "video":

                img_attachment.setVisibility(View.GONE);
                link_preview.setVisibility(View.GONE);
                break;
            case "link":

                break;
            case "status":
                hideAll();
                break;
            default:
                hideAll();
                break;
        }
    }

    private void hideAll(){
        img_attachment.setVisibility(View.GONE);
        video_attachment.setVisibility(View.GONE);
        link_preview.setVisibility(View.GONE);
    }

    private String getMediaType(Tweet tweet){
        if (tweet.entities != null && tweet.entities.media != null && tweet.entities.media.size() > 0) {
            MediaEntity entity = tweet.entities.media.get(0);
            if (entity.type.contains("photo") || entity.type.contains("gif")) {
                return "photo";
            } else if (entity.type.equals("video")) {
                return "video";
            }
        }

        return "status";
    }

    @Override
    public void setVideoPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory, VideoLoadListener listener) {
        video_attachment.setUpPlayer(player, dataSourceFactory, extractorsFactory);
        video_attachment.setVideLoadlistener(listener);
    }
}
