package com.example.carnavalmazatlan.adapters.socialfeed_viewholders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.classes.instagram.Post;
import com.example.carnavalmazatlan.classes.youtube.Channel;
import com.example.carnavalmazatlan.classes.youtube.Video;
import com.example.carnavalmazatlan.custom_components.ProfileHeader;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.example.carnavalmazatlan.custom_components.VideoPreview;
import com.github.kinnonii.timeago.TimeAgo;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.upstream.DataSource;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class YoutubeVideoHolder extends FeedViewHolder {

    private Channel channel_info = null;

    @BindView(R.id.youtube_profile_header)
    ProfileHeader profile_header;

    @BindView(R.id.youtube_txt_status_message)
    TextView txt_status_message;

    @BindView(R.id.youtube_img_attachment)
    ImageView img_attachment;

    @BindView(R.id.youtube_img_video_container)
    RelativeLayout img_video_container;
    
    public YoutubeVideoHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public YoutubeVideoHolder(View itemView, Channel defaultChannelInfo) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.channel_info = defaultChannelInfo;
    }

    @Override
    public void clean() {
        profile_header.resetState();
    }

    @Override
    public void init(Context context, FeedItem item) {
        System.out.println(item.dataYoutube.snippet.resourceId);
        item.dataYoutube.snippet.publishedAt = item.date;
        initProfileHeader(context, item.dataYoutube);
        initStatus(context, item.dataYoutube);
        initMedia(context, item.dataYoutube);
    }

    private void  initProfileHeader(final Context context, final Video video){

        String channel_img_url = "", channel_title = "";

        if (channel_info != null && channel_info.items != null && channel_info.items.size() > 0) {
            channel_img_url = channel_info.items.get(0).snippet.thumbnails.medium.url;
            channel_title = channel_info.items.get(0).snippet.title;
        }
        System.out.println(video.snippet.channelTitle);
        profile_header.setHeaderInfo(channel_img_url, channel_title, video.snippet.publishedAt);
        profile_header.setProvider("youtube");
        profile_header.getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+video.snippet.resourceId.videoId));
                context.startActivity(intent);
            }
        });
    }

    private void initStatus(final Context context, Video video){
        // Set status message in ExpandableTextView
        txt_status_message.setText(video.snippet.title);
    }

    private void initMedia(final Context context, final Video video){
        Glide.with(context)
                .load(video.snippet.thumbnails.medium.url)
                .into(img_attachment);

        img_video_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+video.snippet.resourceId.videoId));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public void setVideoPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory, VideoLoadListener listener) {

    }
}
