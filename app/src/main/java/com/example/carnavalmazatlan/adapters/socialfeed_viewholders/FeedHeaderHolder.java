package com.example.carnavalmazatlan.adapters.socialfeed_viewholders;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.HorizontalRVAdapter;
import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.upstream.DataSource;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class FeedHeaderHolder extends FeedViewHolder{

    @BindView(R.id.rvHorizontalFeed)
    RecyclerView rvHorizontalFeed;

    private HorizontalRVAdapter horizontalRVAdapter;

    public FeedHeaderHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void clean() {

    }

    @Override
    public void init(Context context, FeedItem item) {
        rvHorizontalFeed.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        horizontalRVAdapter = new HorizontalRVAdapter();
        horizontalRVAdapter.setData(item.headerItem);
        rvHorizontalFeed.setAdapter(horizontalRVAdapter);
    }

    @Override
    public void setVideoPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory, VideoLoadListener listener) {

    }
}
