package com.example.carnavalmazatlan.adapters.socialfeed_viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.upstream.DataSource;

/**
 * Created by joeldmtz on 12/26/16.
 */

abstract public class FeedViewHolder extends RecyclerView.ViewHolder {

    public FeedViewHolder(View itemView) {
        super(itemView);
    }

    abstract public void clean();
    abstract public void init(Context context, FeedItem item);
    abstract public void setVideoPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory, VideoLoadListener listener);
}
