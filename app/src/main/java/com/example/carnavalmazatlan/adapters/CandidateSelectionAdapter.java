package com.example.carnavalmazatlan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.app.Candidate;
import com.example.carnavalmazatlan.introslides.CandidateSelectionListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 12/31/16.
 */

public class CandidateSelectionAdapter extends BaseAdapter{

    private Context context;
    private List<Candidate> mDataset;

    public CandidateSelectionAdapter(Context context, List<Candidate> mDataset) {
        this.context = context;
        this.mDataset = mDataset;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDataset.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        CandidateSelectionHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_candidate_selection, parent, false);
            holder = new CandidateSelectionHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (CandidateSelectionHolder) convertView.getTag();
        }

        holder.setSelection(mDataset.get(position).isSelected());
        holder.name.setText(mDataset.get(position).getName());

        return convertView;
    }

    public class CandidateSelectionHolder {

        @BindView(R.id.candidate_layout)
        RelativeLayout layout;

        @BindView(R.id.candidate_img)
        ImageView img;

        @BindView(R.id.candidate_name)
        TextView name;

        @BindView(R.id.candidate_selection)
        View selection;

        public CandidateSelectionHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void setSelection(boolean isSelected){
            if (isSelected) {
                Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in_fast);
                selection.startAnimation(fadeIn);
                selection.setVisibility(View.VISIBLE);
            } else {
                selection.setVisibility(View.GONE);
            }
        }
    }
}
