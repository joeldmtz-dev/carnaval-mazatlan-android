package com.example.carnavalmazatlan.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.activities.EventActivity;
import com.example.carnavalmazatlan.classes.app.Events;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsHolder>{

    private Context ctx;
    private List<Events> eventsList;
    private boolean undoButtonClick; //Variable para eliminar o no eliminar de la base de datos un evento

    public EventsAdapter(Context ctx, List<Events> eventsList){
        this.ctx = ctx;
        this.eventsList = eventsList;
    }

    public class EventsHolder extends RecyclerView.ViewHolder {
        private TextView tvEventName, tvEventTime, tvEventRemainingTime, tvEventLocation;

        public EventsHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent intent = new Intent(v.getContext(), EventActivity.class);
                    intent.putExtra("event", eventsList.get(getAdapterPosition()));
                    v.getContext().startActivity(intent);
                }
            });
            tvEventName = (TextView) itemView.findViewById(R.id.tvEventName);
            tvEventTime = (TextView) itemView.findViewById(R.id.tvEventTime);
            tvEventRemainingTime = (TextView) itemView.findViewById(R.id.tvEventRemainingTime);
            tvEventLocation = (TextView) itemView.findViewById(R.id.tvEventLocation);
        }
    }

    @Override
    public EventsAdapter.EventsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.event_item, parent, false);
        return new EventsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventsHolder holder, int position) {
        holder.tvEventName.setText(eventsList.get(position).getEventName());
        holder.tvEventTime.setText(eventsList.get(position).getEventTime());
        holder.tvEventRemainingTime.setText(eventsList.get(position).getEventRemainingTime());
        holder.tvEventLocation.setText(eventsList.get(position).getEventLocation());
    }

    @Override
    public int getItemCount() {
        return eventsList.size();
    }

    public void remove(int position){
        /*Long id = eventsList.get(position).getId();
        Events events = Events.findById(Events.class, id);
        events.delete();*/
        eventsList.remove(position);
        notifyItemRemoved(position);
        //notifyDataSetChanged();
    }

    public void onItemRemove(final RecyclerView.ViewHolder viewHolder, final RecyclerView recyclerView) {
        final int adapterPosition = viewHolder.getAdapterPosition();
        final Events event = eventsList.get(adapterPosition);
        Snackbar snackbar = Snackbar
                .make(recyclerView, "EVENTO REMOVIDO", Snackbar.LENGTH_LONG)
                .setAction("DESHACER", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventsList.add(adapterPosition, event);
                        notifyItemInserted(adapterPosition);
                        recyclerView.scrollToPosition(adapterPosition);
                        undoButtonClick = true; //Hizo click en el botón
                    }
                });
        snackbar.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View view) {}

            @Override
            public void onViewDetachedFromWindow(View view) {
                if(!undoButtonClick) { //Si no hace click en "DESHACER" entonces si lo puede eliminar de la BD.
                    Events removeEvent = Events.findById(Events.class, event.getId());
                    removeEvent.delete();
                    undoButtonClick = false;
                }
            }
        });
        snackbar.show();
        eventsList.remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
    }
}