package com.example.carnavalmazatlan.adapters.socialfeed_viewholders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.classes.instagram.Post;
import com.example.carnavalmazatlan.custom_components.ProfileHeader;
import com.example.carnavalmazatlan.custom_components.VideoLoadListener;
import com.example.carnavalmazatlan.custom_components.VideoPreview;
import com.github.kinnonii.timeago.TimeAgo;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.upstream.DataSource;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by joeldmtz on 12/26/16.
 */

public class InstagramPostHolder extends FeedViewHolder {

    @BindView(R.id.instagram_profile_header)
    ProfileHeader profile_header;

    @BindView(R.id.instagram_txt_status_message)
    TextView txt_status_message;

    @BindView(R.id.instagram_img_attachment)
    ImageView img_attachment;

    @BindView(R.id.instagram_video_attachment_container)
    VideoPreview video_attachment;

    public InstagramPostHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void clean() {
        img_attachment.setVisibility(View.VISIBLE);
        video_attachment.setVisibility(View.VISIBLE);

        img_attachment.setOnClickListener(null);
        video_attachment.resetState();

        profile_header.resetState();
    }

    @Override
    public void init(Context context, FeedItem item) {
        initProfileHeader(context, item.dataInstagram);
        initStatus(context, item.dataInstagram);
        initMedia(context, item.dataInstagram);
    }

    private void  initProfileHeader(final Context context, final Post post){
        profile_header.setHeaderInfo(post.user.profilePicture, post.user.username, post.createdTime);
        profile_header.setProvider("instagram");
        profile_header.getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.link));
                context.startActivity(intent);
            }
        });
    }

    private void initStatus(final Context context, Post post){
        if (post.caption != null){
            // Set status message in ExpandableTextView
            txt_status_message.setText(post.caption.text);

            // HashTagHelper to handle hastags in the status message
            HashTagHelper mTextHashTagHelper = HashTagHelper.Creator.create(context.getResources().getColor(R.color.colorAccent), new HashTagHelper.OnHashTagClickListener() {
                @Override
                public void onHashTagClicked(String hashTag) {

                }
            });

            // Set the hashtag helper to the status message TextView
            mTextHashTagHelper.handle(txt_status_message);

            // Add links to status message TextView
            Linkify.addLinks(txt_status_message, Linkify.WEB_URLS);

            // Set custom color for links in status message TextView
            //txt_status_message.setLinkTextColor();
        } else {
            txt_status_message.setVisibility(View.GONE);
        }
    }

    private void initMedia(Context context, Post post){
        if (post.type.equals("image")) {

            Glide.with(context)
                    .load(post.images.standardResolution.url)
                    .into(img_attachment);

            video_attachment.setVisibility(View.GONE);
        } else {

            img_attachment.setVisibility(View.GONE);
        }
    }

    @Override
    public void setVideoPlayer(SimpleExoPlayer player, DataSource.Factory dataSourceFactory, ExtractorsFactory extractorsFactory, VideoLoadListener listener) {
        video_attachment.setUpPlayer(player, dataSourceFactory, extractorsFactory);
        video_attachment.setVideLoadlistener(listener);
    }
}
