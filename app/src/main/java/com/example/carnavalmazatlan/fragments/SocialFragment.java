package com.example.carnavalmazatlan.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.VerticalRVAdapter;
import com.example.carnavalmazatlan.apis.ApiFactory;
import com.example.carnavalmazatlan.classes.app.FeedData;
import com.facebook.AccessToken;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.OnSheetDismissedListener;
import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocialFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.social_feed_swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.social_feed_rvFeed)
    RecyclerView rvFeed;

    @BindView(R.id.social_feed_progressBar)
    ProgressBar progressBar;

    @BindView(R.id.social_fab_button)
    FloatingActionButton fab;

    @BindView(R.id.bottomsheet)
    BottomSheetLayout bottomSheet;

    AppCompatCheckBox ck_twitter_filter;
    AppCompatCheckBox ck_instagram_filter;

    boolean twitter_filter_original;
    boolean instagram_filter_original;

    private VerticalRVAdapter adapter;
    private Context ctx;

    private boolean isLoading = false;

    private int requests = 0;

    private static FeedData dataSet = null;

    private boolean isBottomSheedShowing = false;
    private boolean fabPressed = false;

    public SocialFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_social, container, false);

        ButterKnife.bind(this, view);

        initBottomSheet();

        rvFeed.setHasFixedSize(true);
        rvFeed.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        FeedData feed = FeedData.newEmptyInstance();
        adapter = new VerticalRVAdapter(ctx, feed);
        rvFeed.setAdapter(adapter);

        requests = 0;

        if (dataSet == null) {
            initialLoad();
            dataSet = FeedData.newEmptyInstance();
            dataSet.data = adapter.getmDataset();
            dataSet.pagination.nextUntilDate = adapter.getNextUntilDate();
        } else {
            progressBar.setVisibility(View.GONE);
            adapter.setHeaderItem(null);
            adapter.setData(dataSet);
        }

        setInfiniteScroll();

        swipe_refresh_layout.setOnRefreshListener(this);
        swipe_refresh_layout.setColorSchemeResources(R.color.colorAccent);



        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = context;
    }

    private void initBottomSheet(){
        final View bottomSheetView = LayoutInflater.from(ctx).inflate(R.layout.bottom_sheet_social_filter, bottomSheet, false);

        ck_twitter_filter = (AppCompatCheckBox) bottomSheetView.findViewById(R.id.ck_twitter_filter);
        ck_instagram_filter = (AppCompatCheckBox) bottomSheetView.findViewById(R.id.ck_instagram_filter);

        ck_instagram_filter.setChecked(true);
        ck_twitter_filter.setChecked(true);

        twitter_filter_original = ck_twitter_filter.isChecked();
        instagram_filter_original = ck_instagram_filter.isChecked();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBottomSheedShowing) {
                    fab.setImageResource(R.drawable.ic_filled_filter);
                    bottomSheet.dismissSheet();
                    isBottomSheedShowing = false;

                    fabPressed = true;
                    twitter_filter_original = ck_twitter_filter.isChecked();
                    instagram_filter_original = ck_instagram_filter.isChecked();
                } else {
                    fab.setImageResource(R.drawable.ic_check_black);
                    bottomSheet.showWithSheetView(bottomSheetView);
                    isBottomSheedShowing = true;
                }
            }
        });

        bottomSheet.addOnSheetDismissedListener(new OnSheetDismissedListener() {
            @Override
            public void onDismissed(BottomSheetLayout bottomSheetLayout) {
                if (fabPressed) {
                    onRefresh();
                    fabPressed = false;
                } else {
                    ck_twitter_filter.setChecked(twitter_filter_original);
                    ck_instagram_filter.setChecked(instagram_filter_original);
                }

                fab.setImageResource(R.drawable.ic_filled_filter);
                isBottomSheedShowing = false;
            }
        });
    }

    public List<String> getSelectedNetworks(){
        List<String> social_networks = new ArrayList<>();

        if (ck_twitter_filter.isChecked()) {
            social_networks.add("twitter");
        }

        if (ck_instagram_filter.isChecked()) {
            social_networks.add("instagram");
        }

        if (social_networks.size() == 0) {
            social_networks.add("none");
        }

        return  social_networks;
    }

    private void initialLoad(){
        Map<String, String> options = new HashMap<>();

        if (AccessToken.getCurrentAccessToken() != null) {
            options.put("facebook_access_token", AccessToken.getCurrentAccessToken().getToken());
        }

        options.put("networks", TextUtils.join(",", getSelectedNetworks()));

        Call<FeedData> call = ApiFactory.newCarnavalMazatlanInstance().getFeedSocial(options);
        call.enqueue(new Callback<FeedData>() {
            @Override
            public void onResponse(Call<FeedData> call, Response<FeedData> response) {
                if (response.code() == 200) {
                    adapter.setData(response.body());
                    swipe_refresh_layout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);

                    dataSet = FeedData.newEmptyInstance();
                    dataSet.data = adapter.getmDataset();
                    dataSet.pagination.nextUntilDate = adapter.getNextUntilDate();

                    if (response.body().data.size() < 4){
                        requests++;
                        if (requests < ApiFactory.MAX_REQUESTS) {
                            loadMore();
                        }
                    } else {
                        requests = 0;
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedData> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onRefresh() {
        swipe_refresh_layout.setRefreshing(true);
        requests = 0;
        initialLoad();
    }

    public void setInfiniteScroll(){
        BaseAttacher attacher = Mugen.with(rvFeed, new MugenCallbacks() {
            @Override
            public void onLoadMore() {
                loadMore();
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        }).start();

        /* Use this to dynamically turn infinite scroll on or off. It is enabled by default */
        attacher.setLoadMoreEnabled(true);

        /* Use this to change when the onLoadMore() function is called.
        * By default, it is called when the scroll reaches 2 items from the bottom */
        attacher.setLoadMoreOffset(4);
    }

    public void loadMore(){
        Map<String, String> options = new HashMap<>();

        if (AccessToken.getCurrentAccessToken() != null) {
            options.put("facebook_access_token", AccessToken.getCurrentAccessToken().getToken());
        }

        options.put("networks", TextUtils.join(",", getSelectedNetworks()));

        if (adapter.getNextUntilDate() != null) {
            options.put("until", adapter.getNextUntilDate());
        }
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);

        Call<FeedData> call = ApiFactory.newCarnavalMazatlanInstance().getFeedSocial(options);
        call.enqueue(new Callback<FeedData>() {
            @Override
            public void onResponse(Call<FeedData> call, Response<FeedData> response) {
                if (response.code() == 200) {
                    adapter.addMore(response.body());
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);

                    dataSet.data = adapter.getmDataset();
                    dataSet.pagination.nextUntilDate = adapter.getNextUntilDate();

                    if (response.body().data.size() < 4){
                        requests++;
                        if (requests < ApiFactory.MAX_REQUESTS) {
                            loadMore();
                        }
                    } else {
                        requests = 0;
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedData> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && bottomSheet != null && fab != null){
            fab.setImageResource(R.drawable.ic_filled_filter);
            bottomSheet.dismissSheet();
            isBottomSheedShowing = false;
        }
    }
}
