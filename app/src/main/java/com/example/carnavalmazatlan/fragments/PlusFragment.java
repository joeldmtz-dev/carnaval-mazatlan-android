package com.example.carnavalmazatlan.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.activities.SettingsActivity;
import com.example.carnavalmazatlan.adapters.MoreAdapter;
import com.example.carnavalmazatlan.classes.app.More;

import java.util.ArrayList;

public class PlusFragment extends Fragment {

    private Context ctx;

    public PlusFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plus, container, false);
        ListView optionsList = (ListView) view.findViewById(R.id.optionsList);
        ArrayList<More> data = new ArrayList<>();
        data.add(new More("Configuraciones", "Configura tu app"));
        data.add(new More("Créditos", "¡Conoce a los desarrolladores!"));
        data.add(new More("Agradecimientos", ""));
        MoreAdapter moreAdapter = new MoreAdapter(ctx, R.layout.more_item, data);
        optionsList.setAdapter(moreAdapter);
        optionsList.setDivider(null);
        optionsList.setDividerHeight(0);
        optionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        Intent intent = new Intent(ctx, SettingsActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx = context;
    }
}