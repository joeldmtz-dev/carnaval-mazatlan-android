package com.example.carnavalmazatlan.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.EventsAdapter;
import com.example.carnavalmazatlan.classes.app.Events;

import java.util.ArrayList;
import java.util.List;

public class EventsFragment extends Fragment {

    private Context ctx;
    private TextView tvEventsDate;
    private RecyclerView rvEvents;
    private EventsAdapter eventsAdapter;
    private boolean swipable;

    public EventsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);

        tvEventsDate = (TextView) view.findViewById(R.id.tvEventsDate);
        rvEvents = (RecyclerView) view.findViewById(R.id.rvEvents);
        rvEvents.setLayoutManager(new LinearLayoutManager(ctx));
        List<Events> eventsList = new ArrayList<>();
        eventsList.add(new Events("Coronación del la reina del carnaval", "10:00", "3h", "Teatro Angela Peralta", "30/12/2016"));
        eventsList.add(new Events("Quema del mal humor", "10:00", "1h", "Plaza Bandera", "30/12/2016"));
        eventsList.add(new Events("Combate naval", "10:00", "3h 30 min", "Embarcadero del puerto", "30/12/2016"));
        eventsAdapter = new EventsAdapter(ctx, eventsList);
        rvEvents.setAdapter(eventsAdapter);

        final ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT){
            @Override
            public boolean isItemViewSwipeEnabled() {
                return swipable;
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                eventsAdapter.onItemRemove(viewHolder, rvEvents);
            }
        };

        final Button btnEvents = (Button) view.findViewById(R.id.btnEvents);
        btnEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Este código es temporal
                List<Events> eventsList = new ArrayList<>();//Sábado 25/02
                eventsList.add(new Events("Coronación del la reina del carnaval", "18:00", "3h", "Teatro Angela Peralta", "30/12/2016"));
                eventsList.add(new Events("Quema del mal humor", "21:00", "1h", "Plaza Bandera", "30/12/2016"));
                eventsList.add(new Events("Combate naval", "22:00", "3h 30 min", "Embarcadero del puerto", "30/12/2016"));
                eventsAdapter = new EventsAdapter(ctx, eventsList);
                rvEvents.setAdapter(eventsAdapter);
                swipable = false;
            }
        });

        final Button btnMyEvents = (Button) view.findViewById(R.id.btnMyEvents);
        btnMyEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long count = Events.count(Events.class);
                if(count > 0) {
                    List<Events> eventsList = Events.listAll(Events.class);
                    rvEvents.setLayoutManager(new LinearLayoutManager(ctx));
                    eventsAdapter = new EventsAdapter(ctx, eventsList);
                    rvEvents.setAdapter(eventsAdapter);
                    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
                    itemTouchHelper.attachToRecyclerView(rvEvents);
                    swipable = true;
                }else if(count == 0){
                    Toast.makeText(ctx, "Aún no ha agregado eventos de su interés", Toast.LENGTH_SHORT).show();
                }else if(count == -1){
                    Toast.makeText(ctx, "Error en manifest", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx = context;
    }
}
