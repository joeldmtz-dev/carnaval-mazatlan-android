package com.example.carnavalmazatlan.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.TicketAdapter;
import com.example.carnavalmazatlan.classes.FragmentListListener;
import com.example.carnavalmazatlan.classes.app.Place;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by joeldmtz on 1/7/17.
 */

public class TicketFragment extends Fragment {

    @BindView(R.id.ticket_recycler_view)
    RecyclerView recyclerView;

    private Context ctx;
    private FragmentListListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ticket, container, false);
        ButterKnife.bind(this, view);

        List<Place> places = new ArrayList<>();
        places.add(new Place("Angela Peralta"));
        places.add(new Place("Angela Peralta"));
        places.add(new Place("Angela Peralta"));
        places.add(new Place("Angela Peralta"));
        places.add(new Place("Angela Peralta"));
        places.add(new Place("Angela Peralta"));

        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        recyclerView.setAdapter(new TicketAdapter(places, ctx));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (listener != null){
                    listener.onListScrolled();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx =context;
    }

    public void setListener(FragmentListListener listener) {
        this.listener = listener;
    }
}
