package com.example.carnavalmazatlan.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.CandidateSelectionAdapter;
import com.example.carnavalmazatlan.classes.app.Candidate;
import com.example.carnavalmazatlan.introslides.CandidateSelectionListener;
import com.example.carnavalmazatlan.utils.DimenUtils;
import com.github.paolorotolo.appintro.ISlidePolicy;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MaleFragment extends Fragment {

    @BindView(R.id.male_grid_view)
    GridView gridView;

    private Context ctx;
    private CandidateSelectionListener listener;
    private Candidate oldSelection;

    private CandidateSelectionAdapter adapter;
    private static int selection = -1;

    public MaleFragment() {}

    public static MaleFragment newInstance() {
        return new MaleFragment();
    }

    public static MaleFragment newInstance(CandidateSelectionListener listener, int position) {
        MaleFragment fragment = new MaleFragment();
        fragment.setListener(listener);
        selection = position;
        return fragment;
    }

    public void setListener(CandidateSelectionListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_male, container, false);
        ButterKnife.bind(this, view);

        List<Candidate> candidates = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            candidates.add(new Candidate(i + 1, "Candidato " + (i + 1)));
        }

        if (selection >= 0) {
            candidates.get(selection).setSelected(true);
            oldSelection = candidates.get(selection);
        }

        adapter = new CandidateSelectionAdapter(ctx, candidates);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Candidate item = (Candidate) adapter.getItem(position);

                if (oldSelection != null){
                    oldSelection.setSelected(false);
                }

                item.setSelected(true);
                oldSelection = item;
                adapter.notifyDataSetChanged();
                selection = position;

                System.out.println(listener);
                if (listener != null) {
                    listener.onSelection(CandidateSelectionListener.MALE, (int) id, position);
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx = context;
    }
}
