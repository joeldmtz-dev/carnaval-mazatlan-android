package com.example.carnavalmazatlan.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.adapters.VerticalRVAdapter;
import com.example.carnavalmazatlan.apis.ApiFactory;
import com.example.carnavalmazatlan.apis.FacebookApi;
import com.example.carnavalmazatlan.classes.FragmentListListener;
import com.example.carnavalmazatlan.classes.app.FeedData;
import com.example.carnavalmazatlan.classes.app.FeedItem;
import com.example.carnavalmazatlan.classes.facebook.Page;
import com.example.carnavalmazatlan.classes.youtube.Channel;
import com.facebook.AccessToken;
import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by joeldmtz on 12/29/16.
 */

public class StartFeedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.feed_swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.rvFeed)
    RecyclerView rvFeed;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private VerticalRVAdapter adapter;
    private Context ctx;

    private boolean isLoading = false;

    private int requests = 0;

    private static FeedItem header = null;
    private static FeedData dataSet = null;

    private static Page facebookPage = null;
    private static Channel youtubeChannel = null;

    private FragmentListListener listener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_start_feed, container, false);
        ButterKnife.bind(this, view);

        rvFeed.setHasFixedSize(true);
        rvFeed.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        rvFeed.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (listener != null){
                    listener.onListScrolled();
                }
            }
        });

        List<String> data = new ArrayList<>();
        for (int y = 0; y < 10; y++){
            data.add("Hola " + y);
        }

        FeedData feed = FeedData.newEmptyInstance();
        FeedItem item = new FeedItem();
        item.provider = "header";
        item.headerItem = data;
        feed.data.add(item);

        header = item;

        adapter = new VerticalRVAdapter(ctx, feed);
        rvFeed.setAdapter(adapter);

        requests = 0;

        if (dataSet == null) {

            Call<Channel> call = ApiFactory.newYoutubeInstance().getChannelProfile("CarnavalIntMazatlan", "snippet");
            call.enqueue(new Callback<Channel>() {
                @Override
                public void onResponse(Call<Channel> call, Response<Channel> response) {
                    if (response.code() == 200) {
                        youtubeChannel = response.body();
                        adapter.setDefaultYoutubeChannelInfo(youtubeChannel);
                    }
                }

                @Override
                public void onFailure(Call<Channel> call, Throwable t) {
                    t.printStackTrace();
                    youtubeChannel = null;
                }
            });


            FacebookApi.getPageProfile(new com.example.carnavalmazatlan.apis.Callback(){
                @Override
                public void success(Page page) {
                    facebookPage = page;
                    adapter.setDefaultFacebookProfileInfo(page);
                    initialLoad();
                }
            });
        } else {
            progressBar.setVisibility(View.GONE);
            adapter.setDefaultFacebookProfileInfo(facebookPage);
            adapter.setDefaultYoutubeChannelInfo(youtubeChannel);
            adapter.setHeaderItem(null);
            adapter.setData(dataSet);
        }

        setInfiniteScroll();

        swipe_refresh_layout.setOnRefreshListener(this);
        swipe_refresh_layout.setColorSchemeResources(R.color.colorAccent);

        return view;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        ctx = context;
    }

    public void setListListener(FragmentListListener listener){
        this.listener = listener;
    }

    private void initialLoad(){
        Map<String, String> options = new HashMap<>();

        if (AccessToken.getCurrentAccessToken() != null) {
            options.put("facebook_access_token", AccessToken.getCurrentAccessToken().getToken());
        }

        Call<FeedData> call = ApiFactory.newCarnavalMazatlanInstance().getFeed(options);
        call.enqueue(new Callback<FeedData>() {
            @Override
            public void onResponse(Call<FeedData> call, Response<FeedData> response) {
                if (response.code() == 200) {
                    adapter.setData(response.body());
                    swipe_refresh_layout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);

                    dataSet = FeedData.newEmptyInstance();
                    dataSet.data = adapter.getmDataset();
                    dataSet.pagination.nextUntilDate = adapter.getNextUntilDate();

                    if (response.body().data.size() < 4){
                        requests++;
                        if (requests < ApiFactory.MAX_REQUESTS) {
                            loadMore();
                        }
                    } else {
                        requests = 0;
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedData> call, Throwable t) {
                t.printStackTrace();
                swipe_refresh_layout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onRefresh() {
        swipe_refresh_layout.setRefreshing(true);
        requests = 0;
        adapter.setHeaderItem(header);
        initialLoad();
    }

    public void setInfiniteScroll(){
        BaseAttacher attacher = Mugen.with(rvFeed, new MugenCallbacks() {
            @Override
            public void onLoadMore() {
                loadMore();
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        }).start();

        /* Use this to dynamically turn infinite scroll on or off. It is enabled by default */
        attacher.setLoadMoreEnabled(true);

        /* Use this to change when the onLoadMore() function is called.
        * By default, it is called when the scroll reaches 2 items from the bottom */
        attacher.setLoadMoreOffset(4);
    }

    public void loadMore(){
        Map<String, String> options = new HashMap<>();

        if (AccessToken.getCurrentAccessToken() != null) {
            options.put("facebook_access_token", AccessToken.getCurrentAccessToken().getToken());
        }

        if (adapter.getNextUntilDate() != null) {
            options.put("until", adapter.getNextUntilDate());
        }
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);

        Call<FeedData> call = ApiFactory.newCarnavalMazatlanInstance().getFeed(options);
        call.enqueue(new Callback<FeedData>() {
            @Override
            public void onResponse(Call<FeedData> call, Response<FeedData> response) {
                if (response.code() == 200) {
                    adapter.addMore(response.body());
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);

                    dataSet.data = adapter.getmDataset();
                    dataSet.pagination.nextUntilDate = adapter.getNextUntilDate();

                    if (response.body().data.size() < 4){
                        requests++;
                        if (requests < ApiFactory.MAX_REQUESTS) {
                            loadMore();
                        }
                    } else {
                        requests = 0;
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedData> call, Throwable t) {
                t.printStackTrace();
                swipe_refresh_layout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
