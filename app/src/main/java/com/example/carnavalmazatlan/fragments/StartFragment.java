package com.example.carnavalmazatlan.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.example.carnavalmazatlan.R;
import com.example.carnavalmazatlan.classes.FragmentListListener;
import com.konifar.fab_transformation.FabTransformation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartFragment extends Fragment implements FragmentListListener {

    @BindView(R.id.main_fab_button)
    FloatingActionButton fab;

    @BindView(R.id.main_toolbar_footer)
    AHBottomNavigation footer;

    @BindView(R.id.main_pager)
    ViewPager pager;

    Context ctx;
    private int indexTab = 0;

    public StartFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_start, container, false);
        ButterKnife.bind(this, view);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        StartFeedFragment feed_fragment = new StartFeedFragment();
        feed_fragment.setListListener(this);
        adapter.addFragment(feed_fragment);

        adapter.addFragment(new MonigotesFragment());
        adapter.addFragment(new RankingFragment());

        PlaceFragment placeFragment = new PlaceFragment();
        placeFragment.setListener(this);
        adapter.addFragment(placeFragment);

        TicketFragment ticketFragment = new TicketFragment();
        ticketFragment.setListener(this);

        adapter.addFragment(ticketFragment);

        pager.setAdapter(adapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                indexTab = position;
                footer.setCurrentItem(position);

                switch (position) {
                    case 0:
                        fab.setImageResource(R.drawable.ic_lightning_bolt);
                        break;
                    case 1:
                        fab.setImageResource(R.drawable.ic_statue);
                        break;
                    case 2:
                        fab.setImageResource(R.drawable.ic_crown);
                        break;
                    case 3:
                        fab.setImageResource(R.drawable.ic_marker_filled);
                        break;
                    case 4:
                        fab.setImageResource(R.drawable.ic_ticket_filled);
                        break;
                    default:
                        fab.setImageResource(R.drawable.ic_more);
                        break;
                }

                fab.setColorFilter(getResources().getColor(R.color.tw__composer_white));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        pager.setCurrentItem(0);
        fab.setImageResource(R.drawable.ic_lightning_bolt);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FabTransformation.with(fab).transformTo(footer);
                footer.setCurrentItem(indexTab);
            }
        });

        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Feed", R.drawable.ic_lightning_bolt);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Monigotes", R.drawable.ic_statue);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Ranking", R.drawable.ic_crown);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("Sedes", R.drawable.ic_marker_filled);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("Taquilla", R.drawable.ic_ticket_filled);

        footer.setDefaultBackgroundColor(getResources().getColor(R.color.colorPrimary));

        footer.addItem(item1);
        footer.addItem(item2);
        footer.addItem(item3);
        footer.addItem(item4);
        footer.addItem(item5);

        footer.setAccentColor(getResources().getColor(R.color.tw__composer_white));
        footer.setInactiveColor(getResources().getColor(R.color.tw__composer_deep_gray));

        footer.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

        footer.setCurrentItem(0);

        footer.setForceTint(true);

        footer.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                pager.setCurrentItem(position);
                indexTab = position;
                return true;
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        ctx = context;
    }


    @Override
    public void onListScrolled() {
        FabTransformation.with(fab).transformFrom(footer);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && fab != null && footer != null){
            FabTransformation.with(fab).transformFrom(footer);
        }
    }
}
